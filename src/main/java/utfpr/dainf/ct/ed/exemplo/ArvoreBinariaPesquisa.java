package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Exemplo de implementação de árvore binária de pesquisa.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     *
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     *
     * @param pai O nó pai.
     *
     * protected void setPai(ArvoreBinariaPesquisa<E> pai) { super.pai = pai; }
     *
     * /**
     * Retorna o nó pai deste nó.
     *
     * @return O nó pai.
     *
     * protected ArvoreBinariaPesquisa<E> getPai() { return
     * (ArvoreBinariaPesquisa) super.pai; }
     */
    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     *
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        ArvoreBinariaPesquisa<E> no = this;
        ArvoreBinariaPesquisa<E> resultado = null;

        while (no != null && resultado == null) {

            if (no.getValor().compareTo(valor) == 0) {
                resultado = no;
            }

            if (no.getEsquerda() != null
                && no.getValor().compareTo(valor) > 0) {
                no = (ArvoreBinariaPesquisa) no.getEsquerda();
            }
            else if (no.getDireita() != null
                     && no.getValor().compareTo(valor) < 0) {
                no = (ArvoreBinariaPesquisa) no.getDireita();
            }
            else {
                no = null;
            }
        }

        return resultado;
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     *
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() {
        ArvoreBinariaPesquisa<E> resultado = this;

        while (resultado.getEsquerda() != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.getEsquerda();
        }

        return resultado;
    }

    /**
     * Retorna o nó da árvore com o maior valor.
     *
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E> resultado = this;

        while (resultado.getDireita() != null) {
            resultado = (ArvoreBinariaPesquisa) resultado.getDireita();
        }

        return resultado;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     *
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {
     * @null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa resultado;

        if (no != null && no.getDireita() != null) {
            resultado = (ArvoreBinariaPesquisa) no.getDireita();
            return resultado.getMinimo();
        }

        resultado = (ArvoreBinariaPesquisa) no.getPai();

        while (resultado != null && resultado.getDireita() == no) {
            no = resultado;
            resultado = (ArvoreBinariaPesquisa) no.getPai();
        }

        return resultado;
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     *
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {
     * @null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        ArvoreBinariaPesquisa resultado;

        if (no != null && no.getEsquerda() != null) {
            resultado = (ArvoreBinariaPesquisa) no.getEsquerda();
            return resultado.getMaximo();
        }

        resultado = (ArvoreBinariaPesquisa) no.getPai();

        while (resultado != null && resultado.getEsquerda() == no) {
            no = resultado;
            resultado = (ArvoreBinariaPesquisa) no.getPai();
        }

        return resultado;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     *
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
        ArvoreBinariaPesquisa<E> no = this;
        ArvoreBinariaPesquisa<E> newNo = new ArvoreBinariaPesquisa<>(valor);
        ArvoreBinariaPesquisa<E> paiAux = no;

        while (no != null) {
            paiAux = no;
            if (no.getEsquerda() != null
                && no.getValor().compareTo(valor) > 0) {
                no = (ArvoreBinariaPesquisa) no.getEsquerda();
            }
            else {
                no = (ArvoreBinariaPesquisa) no.getDireita();
            }
        }

        newNo.setPai(paiAux);

        if (paiAux.getValor().compareTo(valor) > 0) {
            paiAux.setEsquerda(newNo);
        }
        else {
            paiAux.setDireita(newNo);
        }

        return newNo;
    }

    /**
     * Exclui o nó especificado da árvore. Se a raiz for excluída, retorna a
     * nova raiz.
     *
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
        if (no.getPai() != null) {
            if (no.getDireita() != null && no.getEsquerda() != null) {
                E aux = this.sucessor(no).getValor();
                this.exclui(this.sucessor(no));
                no.setValor(aux);
            }
            else {
                if (no.getDireita() == null && no.getEsquerda() == null) {
                    if (no.getPai().getDireita() == no) {
                        no.getPai().setDireita(null);
                    }
                    else {
                        no.getPai().setEsquerda(null);
                    }
                }
                else if (no.getDireita() != null) {
                    if (no.getPai().getDireita() == no) {
                        no.getPai().setDireita(no.getDireita());
                        no.getDireita().setPai(no.getPai());
                    }
                    else {
                        no.getPai().setEsquerda(no.getDireita());
                        no.getDireita().setPai(no.getPai());
                    }
                }
                else if (no.getEsquerda() != null) {
                    if (no.getPai().getDireita() == no) {
                        no.getPai().setDireita(no.getEsquerda());
                        no.getEsquerda().setPai(no.getPai());
                    }
                    else {
                        no.getPai().setEsquerda(no.getEsquerda());
                        no.getEsquerda().setPai(no.getPai());
                    }
                }
            }
        }
        else {
            if (no.getDireita() != null && no.getEsquerda() != null) {
                E aux = this.sucessor(no).getValor();
                this.exclui(this.sucessor(no));
                no.setValor(aux);
            }
            else {
                if (no.getDireita() != null) {
                    no.getDireita().setPai(null);
                    return (ArvoreBinariaPesquisa) no.getDireita();
                }
                else {
                    no.getEsquerda().setPai(null);
                    return (ArvoreBinariaPesquisa) no.getEsquerda();
                }
            }
        }

        return this;
    }
}
